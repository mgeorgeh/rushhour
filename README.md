# README #

RushHour is a solver for a kind of [sliding block puzzles](https://en.wikipedia.org/wiki/Rush_Hour_(board_game)) with the same name. It takes as input a board state specified by a text file (examples in the testCases folder) and either animates or prints a solution to the puzzle.

### Usage ###

Compile with stack, then run with `stack exec rush-hour-exe testCases/RHTest8.txt solve` or `stack exec rush-hour-exe testCases/RHTest1.txt animate`
