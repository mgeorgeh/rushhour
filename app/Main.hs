module Main where

import Pieces (solve, intermediates, Board)
import Parser (parse,boardToStr, solToStr)
import System.Environment
import Control.Concurrent (threadDelay)
import Control.Monad

main :: IO ()
main = process =<< getArgs

process (f:"animate":[]) = animateSol f
process (f:"solve":[]) = printSol f
process _ = putStrLn "Error: Invalid input"

animateSol :: FilePath -> IO ()
animateSol f = animate.intermediates.parse =<< readFile f

printSol :: FilePath -> IO ()
printSol f = putStrLn.solToStr.solve.parse =<< readFile f

animate :: [Board] -> IO ()
animate bs = makeSpace >> moveCursorBack >> mapM_ render bs

render :: Board -> IO ()
render b = do
  replicateM_ 9 (eraseLine >> moveCursorUp)
  putStrLn.boardToStr $ b
  threadDelay 250000

makeSpace = putStr "\n\n\n\n\n\n\n\n\n\n"
moveCursorUp = putStr "\ESC[1A"
moveCursorBack = putStr "\ESC[1000D"
eraseLine = putStr "\ESC[K"
