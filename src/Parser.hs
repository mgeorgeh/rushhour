module Parser
(boardToStr,
 parse,
 solToStr)
where

import Pieces
import Control.Monad (liftM2)
import qualified Data.Vector as V
import qualified Data.List  as L (transpose, unlines)


{-return a vector of pieces from contents of .txt file-}
parse :: String -> Board
parse = getBoard . V.fromList . map V.fromList . tail . init . lines

{-today's getBoard function brought to you by
 the "I just learned about the reader monad" foundation-}
getBoard :: V.Vector (V.Vector Char) -> Board
getBoard = liftM2 (V.++) horizontals verticals

horizontals, verticals :: V.Vector (V.Vector Char) -> V.Vector Piece
horizontals = getPieces tripleToPieceH '[' ']'

verticals = getPieces tripleToPieceV '‾' '_'.trainspose

{-assumes that delimiter indices occur at sensible places-}
tripleToPieceV :: (Int,Int,Int) -> Piece
tripleToPieceV (a,b,c)
  | c - b == 2 = Long (x,y) (x,y - 1) (x,y - 2)
  | otherwise = Short (x,y) (x,y - 1)
  where
    x = (a - 1) `div` 3
    y = 5 - b

tripleToPieceH :: (Int,Int,Int) -> Piece
tripleToPieceH (a,b,c)
  | c - b == 8 = Long (x,y) (x+1,y) (x+2,y)
  | otherwise = Short (x,y) (x+1,y)
  where
    x = b `div` 3
    y = 5 - a

{- parse a vector of strings into a vector of pieces:
Index the vector ->
turn the rows into vectors of pairs of indices of delimiter locations ->
prepend the index of the row onto each pair of indices ->
turn the 3-tuples into pieces.
[[Char]] -> [(index,[Char])] -> [(i,[(i1,i2)])] -> [[(i,i1,i2)]] -> [(i,i1,i2)] -> [Piece]-}
getPieces :: ((Int,Int,Int) -> Piece) -> -- tripleToPiece function
             Char -> --left delimiter
             Char -> --right delimiter
             V.Vector (V.Vector Char) -> -- source chars
             V.Vector Piece
getPieces f l r xs = V.concatMap (\(a,bs) -> V.map (\(b,c) -> f (a,b,c)) bs)
                                 (V.map (\(i,as) -> (i,V.zip (V.elemIndices l as)
                                                             (V.elemIndices r as)))
                                        (V.indexed xs))

solToStr :: V.Vector (V.Vector Move) -> String
solToStr = L.unlines . map showMove . V.toList . V.head

showMove :: Move -> String
showMove (p1,p2) = (show.V.toList.getCoords $ p1) ++ (postFix p1 p2) where
  postFix p1 p2
    | x1 p1 == x1 p2 && y1 p1 < y1 p2 = " -> Up"
    | x1 p1 == x1 p2 && y1 p1 > y1 p2 = " -> Down"
    | x1 p1 < x1 p2 && y1 p1 == y1 p2 = " -> Right"
    | x1 p1 > x1 p2 && y1 p1 == y1 p2 = " -> Left"

boardToStr :: Board -> String
boardToStr b = V.toList (V.fromList "------------------\n" V.++
      vUnlines (shadeTarget (V.foldr' insertPiece emptyBoard b)) V.++
                V.fromList "------------------\n")

{-make the target block look different than the other blocks-}
shadeTarget :: V.Vector (V.Vector Char) -> V.Vector (V.Vector Char)
shadeTarget xs = updateIndices xs [(2,replaceAll '=' '+')]

insertPiece :: Piece -> V.Vector (V.Vector Char) -> V.Vector (V.Vector Char)
insertPiece p b
  | isVertical p = case p of
    Long _ _ _-> trainspose (updateIndices t [(3*x,    replaceSlice (5 - y) 3 "|||"),
                                           (3*x + 1,replaceSlice (5 - y) 3 "‾ _"),
                                           (3*x + 2,replaceSlice (5 - y) 3 "|||")])

    Short _ _ -> trainspose (updateIndices t [(3*x,    replaceSlice (5 - y) 2 "||"),
                                            (3*x + 1,replaceSlice (5 - y) 2 "‾_"),
                                            (3*x + 2,replaceSlice (5 - y) 2 "||")])
  | otherwise = case p of
    Long _ _ _ -> updateIndices b [(5 - y,replaceSlice (3*x) 9 "[=======]")]
    Short _ _ -> updateIndices b [(5 - y,replaceSlice (3*x) 6 "[====]")]
    where
      t = trainspose b
      x = x1 p
      y = y1 p

replaceAll :: Char -> Char -> V.Vector Char -> V.Vector Char
replaceAll a b = V.map (\x -> if x == a then b else x)

updateIndices :: V.Vector a -> [(Int, a -> a)] -> V.Vector a
updateIndices as bs = as V.// map (\(i,f) -> (i,f (as V.! i))) bs

{-starting index, length-}
replaceSlice :: Int -> --starting index
  Int -> --length of slice to replace
  [a] -> --elements to be inserted
  V.Vector a -> --source vector
  V.Vector a
replaceSlice a b new v = V.take a v V.++ V.fromList new V.++ V.drop (a + b) v

emptyBoard = V.fromList (map V.fromList ["                  ",
                                         "                  ",
                                         "                  ",
                                         "                  ",
                                         "                  ",
                                         "                  "])

vUnlines = V.concatMap (V.++ V.fromList "\n")

{-Data.Vector doesn't have a transpose function, so we hijack the one from Data.List-}
trainspose :: V.Vector (V.Vector a) -> V.Vector (V.Vector a)
trainspose = V.fromList . map V.fromList . L.transpose . V.toList . V.map V.toList
