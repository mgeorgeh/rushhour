module Pieces where

import Data.Vector (Vector)
import qualified Data.Vector as V

import Data.Set (Set)
import qualified Data.Set as S

type Coord = (Int,Int)
data Piece = Short Coord Coord
           | Long Coord Coord Coord deriving (Eq, Ord, Show)

type Move = (Piece,Piece)
type Board = Vector Piece

data GameState = GameState {
  board :: Board,
  sourceMoves :: Vector Move
}

solved :: GameState -> Bool
solved = V.elem (Short (4,3) (5,3)) . board
-- vector updates are a little sketchy.
-- datatypes still a little weird
-- code very ugly
{-return a vector of all minimum length solutions to a board state-}
solve :: Board -> Vector (Vector Move)
solve x = solve' (V.singleton (GameState x V.empty)) S.empty

-- states being considered, states already seen
solve' :: Vector GameState -> Set Board -> Vector (Vector Move)

solve' b seens
  | not (V.null sols) = V.map sourceMoves sols
  | otherwise = solve' (V.filter (\g -> not (S.member (board g) seens)) (step b))
                       (S.union seens newBoards)
  where sols = V.filter solved b
        newBoards = V.foldl' (\acc x -> S.insert x acc) S.empty (V.map board b)

{-return a list of all board states between start and solution-}
intermediates :: Board -> [Board]
intermediates b = V.toList (V.scanl (flip applyMove) b (V.head (solve b)))

{-return all boards that can be reached with one move from a vector of boards-}
step :: Vector GameState -> Vector GameState
step xs = nubBy (equating board) (V.concatMap nexts xs)

{-return the board states that can be reached in one move from a given baord-}
nexts :: GameState -> Vector GameState
nexts (GameState b ms) = V.map (\a -> GameState (applyMove a b) (ms V.++ V.singleton a))
                               (moves b)

{-return the legal moves that can be made on a given board-}
moves :: Board -> Vector Move
moves b = V.concatMap (\x -> (V.map (\y -> (x,y)) (positions x b))) b

{-assumes that the given move can be applied-}
applyMove :: Move -> Board -> Board
applyMove (x,y) b = b V.// [(fromJust (V.elemIndex x b),y)]
  where
    fromJust (Just a) = a

{-return the positions that a given piece can move to on a board
  assumes piece coordinates are listed top to bottom, left to right-}
positions :: Piece -> Board -> Vector Piece
positions p b
  | isHorizontal p = checkLeft p b V.++ checkRight p b
  | otherwise = checkUp p b V.++ checkDown p b
  where

    checkLeft p b
      | (x1 p == 0) || ((x1 p - 1,y1 p) `inAny` b) = V.empty
      | otherwise = V.singleton (moveLeft p)

    checkRight p b
      | (xl p == 5) || ((xl p + 1,yl p) `inAny` b) = V.empty
      | otherwise = V.singleton (moveRight p)

    checkUp p b
      | (y1 p == 5) || ((x1 p, y1 p + 1) `inAny` b) = V.empty
      | otherwise = V.singleton (moveUp p)

    checkDown p b
      | (yl p == 0) || ((xl p, yl p - 1) `inAny` b) = V.empty
      | otherwise = V.singleton (moveDown p)


moveUp, moveDown, moveLeft, moveRight :: Piece -> Piece
moveUp (Short (x1,y1) (x2,y2)) = Short (x1,y1+1) (x2,y2+1)
moveUp (Long (x1,y1) (x2,y2) (x3,y3)) = Long (x1,y1+1) (x2,y2+1) (x3,y3+1)

moveDown (Short (x1,y1) (x2,y2)) = Short (x1,y1-1) (x2,y2-1)
moveDown (Long (x1,y1) (x2,y2) (x3,y3)) = Long (x1,y1-1) (x2,y2-1) (x3,y3-1)

moveLeft (Short (x1,y1) (x2,y2)) = Short (x1-1,y1) (x2-1,y2)
moveLeft (Long (x1,y1) (x2,y2) (x3,y3)) = Long (x1-1,y1) (x2-1,y2) (x3-1,y3)

moveRight (Short (x1,y1) (x2,y2)) = Short (x1+1,y1) (x2+1,y2)
moveRight (Long (x1,y1) (x2,y2) (x3,y3)) = Long (x1+1,y1) (x2+1,y2) (x3+1,y3)

inAny :: (Int,Int) -> Board -> Bool
inAny = elemBy (\x y -> V.elem y (getCoords x))

{-functions missing from the vector library-}
nubBy :: (a -> a -> Bool) -> Vector a -> Vector a
nubBy eq = V.foldl' (\bs x -> if elemBy eq x bs then bs else V.cons x bs) V.empty

elemBy :: (a -> t -> Bool) -> t -> Vector a -> Bool
elemBy eq y = V.any (`eq` y)

equating :: Eq b => (a -> b) -> a -> a -> Bool
equating f = \a b -> f a == f b

getCoords :: Piece -> Vector Coord
getCoords (Short a b) = V.fromList [a,b]
getCoords (Long a b c) = V.fromList [a,b,c]

x1, y1, x2, y2, xl, yl :: Piece -> Int
x1 (Short (x,_) _) = x
x1 (Long (x,_) _ _) = x

y1 (Short (_,y) _) = y
y1 (Long (_,y) _ _) = y

x2 (Short _ (x,_)) = x
x2 (Long _ (x,_) _) = x

y2 (Short _ (_,y)) = y
y2 (Long _ (_,y) _) = y

xl (Short _ (x,_)) = x
xl (Long _ _ (x,_)) = x

yl (Short _ (_,y)) = y
yl (Long _ _ (_,y)) = y

isHorizontal p = y1 p == y2 p
isVertical p = x1 p == x2 p
